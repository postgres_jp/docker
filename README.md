# postgres_jp ( registry.gitlab.com/postgres_jp/docker )

PostgreSQLのDockerイメージの日本語バージョンです。

イメージ内のDBクラスタのlocaleやcharset、collateの設定を日本語にしています。

正式なDockerイメージはUTF-8をサポートしているので日本語の文字列は普通に収納できますが、
デフォルトの照合順序（collation）は日本語を対応していないため、日本語文字列を順番に
並べるときに奇妙な順序になりがちです。このイメージを使うと日本語らしい順番になります。

正式なDockerイメージを簡単にラッピングしたものなので、
イメージの詳細についての説明は以下のページでご参照ください。

https://hub.docker.com/_/postgres

## 使い方の例

以下のようにDockerリポジトリをご自由にお使いください。

```
# PostgreSQL 9.6をデーモンとして実行し、デフォルトのポート番の5432をさらす
docker run -d -p 5432:5432 registry.gitlab.com/postgres_jp/docker:9.6

# DBに接続してみる
psql -h localhost -p 5432 -d postgres -U postgres
```

## デフォルト設定

* ポート番: 5432
* ユーザー名: postgres （ `POSTGRES_USER` という環境変数で設定可能）
* DB名: ユーザー名と一緒 （ `POSTGRES_DB` という環境変数で設定可能）
* パスワード: （空）（ `POSTGRES_PASSWORD` という環境変数で設定可能。空のままだと、localhostからしか接続できないのでご注意ください。）

## サポートされているバージョン（イメージのタグとして使えます）

* 11
* 11.2
* 10
* 10.7
* 9
* 9.6
* 9.6.12
* 9.5
* 9.5.16
* 9.4
* 9.4.21

## ライセンス
ライセンスについては https://www.postgresql.org/about/licence/ をご参照ください。