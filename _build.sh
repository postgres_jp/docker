#!/bin/sh -xe

TAG="$1"

TEMP_DOCKERFILE="Dockerfile.$(date '+%s').tmp"
REPO="registry.gitlab.com/postgres_jp/docker"

cat Dockerfile.template | \
sed "s@POSTGRES_VER_TAG@$TAG@g" > "$TEMP_DOCKERFILE"

docker build -f "$TEMP_DOCKERFILE" -t "$REPO":"$TAG" .
rm "$TEMP_DOCKERFILE"
docker login registry.gitlab.com -u gitlab-ci-token -p $CI_BUILD_TOKEN
docker push "$REPO":"$TAG"
