#!/bin/sh -xe

./_build.sh 11
./_build.sh 11.2
./_build.sh 10
./_build.sh 10.7
./_build.sh 9
./_build.sh 9.6
./_build.sh 9.6.12
./_build.sh 9.5
./_build.sh 9.5.16
./_build.sh 9.4
./_build.sh 9.4.21
./_build.sh latest
